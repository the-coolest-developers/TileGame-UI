﻿using TileGame.Application.Commands;

namespace TileGame.Application.Responses
{
    public interface IQueryResponse<TValue>
    {
        OperationStatus Status { get; }

        TValue Value { get; }
    }
}
