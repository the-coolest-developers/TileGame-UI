﻿using TileGame.Application.Commands;

namespace TileGame.Application.Responses
{
    public record class QueryResponse<TValue>(OperationStatus Status, TValue Value = default) : IQueryResponse<TValue>;
}
