﻿using TileGame.Application.Contracts.Http;
using TileGame.Infrastructure.Common.Enums;

namespace TileGame.Application.Responses.Mappers
{
    public class OperationMapper : IOperationMapper
    {
        private readonly IRequestStatusMapper _requestStatusMapper;

        public OperationMapper(IRequestStatusMapper requestStatusMapper)
        {
            _requestStatusMapper = requestStatusMapper;
        }

        public ICommandResponse GetCommandResponse(ResponseStatus responseStatus)
        {
            var requestStatus = _requestStatusMapper.GetOperationStatus(responseStatus);
            return new CommandResponse(requestStatus);
        }

        public IQueryResponse<TResponse> GetQueryResponse<TResponse>(IHttpResponse<TResponse> httpResponse)
        {
            var responseStatus = _requestStatusMapper.GetOperationStatus(httpResponse.ResponseStatus);
            return new QueryResponse<TResponse>(responseStatus, httpResponse.ResponseBody);
        }
    }
}
