﻿using TileGame.Application.Contracts.Http;
using TileGame.Infrastructure.Common.Enums;

namespace TileGame.Application.Responses.Mappers
{
    public interface IOperationMapper
    {
        public ICommandResponse GetCommandResponse(ResponseStatus responseStatus);
        public IQueryResponse<TResponse> GetQueryResponse<TResponse>(IHttpResponse<TResponse> httpResponse);
    }
}
