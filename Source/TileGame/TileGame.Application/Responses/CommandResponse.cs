﻿using TileGame.Application.Commands;

namespace TileGame.Application.Responses
{
    public record class CommandResponse(OperationStatus Status) : ICommandResponse;
}
