﻿using TileGame.Application.Commands;

namespace TileGame.Application.Responses
{
    public interface ICommandResponse
    {
        OperationStatus Status { get; }
    }
}
