﻿using TileGame.Application.Contracts.Http;
using TileGame.Application.Responses;
using TileGame.Application.Responses.Mappers;
using TileGame.Application.SingleSignOn.AuthenticateAccount;
using TileGame.Infrastructure.Common.Configuration;

namespace TileGame.Application.SingleSignOn
{
    public class SingleSignOnService : ISingleSignOnService
    {
        private readonly IHttpRequestSender _httpRequestSender;
        private readonly IOperationMapper _operationMapper;

        private readonly ServiceUrlConfiguration _backendUrlConfiguration;

        public SingleSignOnService(
            IHttpRequestSender httpRequestSender,
            IOperationMapper responseMapper,
            ServiceUrlConfiguration backendUrlConfiguration)
        {
            _httpRequestSender = httpRequestSender;
            _operationMapper = responseMapper;
            _backendUrlConfiguration = backendUrlConfiguration;
        }

        public ICommandResponse AuthenticateAccount(AuthenticateAccountCommand request)
        {
            var fullUrl = $"{_backendUrlConfiguration.SingleSignOn}/login";
            var response = _httpRequestSender.Post<AuthenticateAccountResponse>(fullUrl, request);

            return _operationMapper.GetCommandResponse(response.ResponseStatus);
        }
    }
}
