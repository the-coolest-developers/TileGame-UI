﻿using TileGame.Application.Responses;
using TileGame.Application.SingleSignOn.AuthenticateAccount;

namespace TileGame.Application.SingleSignOn
{
    public interface ISingleSignOnService
    {
        ICommandResponse AuthenticateAccount(AuthenticateAccountCommand request);
    }
}
