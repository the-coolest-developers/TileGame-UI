﻿namespace TileGame.Application.SingleSignOn.AuthenticateAccount;

public record class AuthenticateAccountResponse(string? Token);
