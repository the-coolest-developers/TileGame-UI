﻿namespace TileGame.Application.SingleSignOn.AuthenticateAccount;

public record class AuthenticateAccountCommand(string Email, string Password);
