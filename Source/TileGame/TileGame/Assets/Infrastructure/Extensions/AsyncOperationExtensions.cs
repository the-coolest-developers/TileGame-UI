﻿using UnityEngine;

namespace TileGame.Infrastructure.Extensions
{
    public static class AsyncOperationExtensions
    {
        public static void Wait(this AsyncOperation asyncOperation)
        {
            while (!asyncOperation.isDone) ;
        }
    }
}
