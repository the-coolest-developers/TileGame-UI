﻿using Newtonsoft.Json;
using TileGame.Application.Contracts.Json;

namespace TileGame.Infrastructure.Json
{
    public class UnityJsonConverter : IJsonConverter
    {
        public TResponse Deserialize<TResponse>(string json) => JsonConvert.DeserializeObject<TResponse>(json);

        public string Serialize<TResponse>(TResponse response) => JsonConvert.SerializeObject(response);
    }

}
