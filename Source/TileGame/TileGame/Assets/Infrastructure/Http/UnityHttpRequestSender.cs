﻿using System.Text;
using TileGame.Application.Contracts.Http;
using TileGame.Application.Contracts.Json;
using TileGame.Infrastructure.Common.Enums;
using TileGame.Infrastructure.Extensions;
using UnityEngine.Networking;

namespace TileGame.Infrastructure.Http
{
    public class UnityHttpRequestSender : IHttpRequestSender
    {
        private readonly IJsonConverter _jsonConverter;

        public UnityHttpRequestSender(IJsonConverter jsonConverter)
        {
            _jsonConverter = jsonConverter;
        }

        public IHttpResponse<TResponse> Get<TResponse>(string url)
        {
            using var unityWebRequest = UnityWebRequest.Get(url);
            unityWebRequest.SendWebRequest().Wait();

            var statusCode = unityWebRequest.responseCode;
            var responseBody = _jsonConverter.Deserialize<TResponse>(unityWebRequest.downloadHandler.text);

            var response = new HttpResponse<TResponse>((ResponseStatus)statusCode, responseBody);

            return response;
        }

        public IHttpResponse<TResponse> Post<TResponse>(string url, object requestBody)
        {
            var serializedBody = _jsonConverter.Serialize(requestBody);

            using var unityWebRequest = new UnityWebRequest(url, "POST")
            {
                uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(serializedBody))
                {
                    contentType = "application/json"
                },
                downloadHandler = new DownloadHandlerBuffer()
            };

            unityWebRequest.SendWebRequest().Wait();

            var responseBody = _jsonConverter.Deserialize<TResponse>(unityWebRequest.downloadHandler.text);

            var response = new HttpResponse<TResponse>((ResponseStatus)unityWebRequest.responseCode, responseBody);

            return response;
        }
    }

}
