﻿using TileGame.Application.SingleSignOn;
using TileGame.Application.SingleSignOn.AuthenticateAccount;
using UnityEngine;
using Zenject;

public class TestMonoBehavior : MonoBehaviour
{
    [Inject]
    public ISingleSignOnService SingleSignOnService { get; set; }

    void Start()
    {
        /*IJsonConverter jsonConverter = new UnityJsonConverter();
        IHttpRequestSender httpRequestSender = new UnityHttpRequestSender(jsonConverter);

        IRequestStatusMapper requestStatusMapper = new RequestStatusMapper();
        IOperationMapper operationMapper = new OperationMapper(requestStatusMapper);
        var serviceUrlConfiguration = new ServiceUrlConfiguration("https://localhost:44317");

        ISingleSignOnService ssoService = new SingleSignOnService(httpRequestSender, operationMapper, serviceUrlConfiguration);
        */
        var authenticateAccount = new AuthenticateAccountCommand("abcde@gmail.com", "test");

        var response = SingleSignOnService.AuthenticateAccount(authenticateAccount);
    }
}
