using UnityEngine;
using UnityEngine.UI;

public class HoveredAnimation : MonoBehaviour
{
    public float _changingTime = 0.01f;

    private Color _color;

    private bool isEntered;

    public Image _imageComponent;

    void Start()
    {
        _color = new Color(0.8705882f, 0.07058824f, 0.3490196f, 0f);

        _imageComponent.color = _color;
    }

    void Update()
    {
        ChangeGlowColor();
    }

    public void OnEnter()
    {
        _color = new Color(0.8705882f, 0.07058824f, 0.3490196f, 1f);

        isEntered = true;
    }

    public void OnExit()
    {
        _color = new Color(0.8705882f, 0.07058824f, 0.3490196f, 0f);

        isEntered = false;
    }

    public void PointerDown()
    {
        _color = new Color(0.5647059f, 0.8156863f, 0.8313726f, 1f);
    }

    public void PointerUp()
    {
        var colorAlpha = isEntered ? 1f : 0f;

        _color = new Color(0.8705882f, 0.07058824f, 0.3490196f, colorAlpha);
    }

    public void ChangeGlowColor()
    {
        _imageComponent.color = Color.Lerp(_imageComponent.color, _color, Mathf.PingPong(Time.time, _changingTime));
    }
}
