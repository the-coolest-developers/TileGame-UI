using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.PlasticSCM.Editor.WebApi;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class SessionListLoadingMenu : MonoBehaviour
{
    public GameObject _sessionsHolder;
    public GameObject _sessionPrefab;

    public int CurrentPage = 1; 

    private GameObject currentObject;
    private int _sessionsAmmount;

    List<Session> sessionsList = new List<Session>();

    private int number = 1;

    public int lastPage;

    private TMP_Text PageText;

    private class Session
    {
        public string name;
        public bool isPasworded;
        public int playersAmmount;
    }

    void Start()
    {
        PageText = GameObject.Find("PageNumber").GameObject().gameObject.ConvertTo<TMP_Text>();
        PageText.text = CurrentPage.ToString();


        for (int i = 0; i < 200; i++)
        {
            Session session = new Session();

            session.name = "NAME";
            session.playersAmmount = Random.Range(1, 6);

            int x = Random.Range(0, 2);

            if (x == 1)
            {
                session.isPasworded = true;
            }
            else
            {
                session.isPasworded = false;
            }

            sessionsList.Add(session);
        }


        lastPage = sessionsList.Count / 15;

        if (sessionsList.Count % 15 > 0)
        {
            lastPage += 1;
        }

        LoadPage();
    }
   
    public void LoadPage()
    {
        ClearPage();

        number = CurrentPage - 1;       
        number *= 15;
        number += 1;

        if (CurrentPage == lastPage)
        {
            _sessionsAmmount = sessionsList.Count - ( lastPage -1 ) * 15;

        }
        else
        {
        
            _sessionsAmmount = 15;
        }

        int x = CurrentPage - 1;
        x *= 15;

        for (int i = 0; i < _sessionsAmmount; i++)
        {
            currentObject = Instantiate(_sessionPrefab, _sessionsHolder.transform);

            currentObject.transform.GetChild(3).transform.GetChild(0).GameObject().gameObject.ConvertTo<TMP_Text>().text = number.ToString();
            number++;

            if (sessionsList[x + i].isPasworded == false)
            {
                currentObject.transform.GetChild(2).GameObject().gameObject.ConvertTo<Image>().enabled = false;
            }

            currentObject.transform.GetChild(3).transform.GetChild(2).GameObject().gameObject.ConvertTo<TMP_Text>().text = sessionsList[x + i].playersAmmount.ToString() + "/" + "6";

        }

        PageText.text = CurrentPage.ToString();

    }

    public void ClearPage()
    { 
        foreach ( Transform child in GameObject.Find("SessionsHolder").transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
