using UnityEngine;

public class RotationScript : MonoBehaviour
{
    public float _rotationSpeed;

    void Update()
    {
        transform.Rotate(Vector3.forward * Time.deltaTime * _rotationSpeed);
    }
}
