using TMPro;
using UnityEngine;

public class TextHoveredScript : MonoBehaviour
{
    public TMP_Text textMesh;

    private bool isEntered;

    void Start()
    {
        textMesh.color = new Color(0.8705882f, 0.07058824f, 0.3490196f, 1f);
        isEntered = false;
    }

    public void PointerEnter()
    {
        textMesh.color = new Color(0f, 0f, 0f, 1f);
        isEntered = true;
    }

    public void PointerExit()
    {
        textMesh.color = new Color(0.8705882f, 0.07058824f, 0.3490196f, 1f);
        isEntered = false;
    }

    public void PointerDown()
    {
        textMesh.color = new Color(0f, 0f, 0f, 1f);
    }

    public void PointerUp()
    {
        textMesh.color = isEntered
            ? new Color(0f, 0f, 0f, 1f)
            : new Color(0.8705882f, 0.07058824f, 0.3490196f, 1f);
    }
}
