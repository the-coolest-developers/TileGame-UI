using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class ButtonLoadScene : MonoBehaviour
{
    private Canvas _canvas;


    public void ApplyButtonAction()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PlayButtonAction()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void LogInButtonAction()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ToLogInButtonAction()
    {
        GameObject.Find("RegistrationCanvas").SetActive(false);

        foreach (Canvas canvas in Resources.FindObjectsOfTypeAll<Canvas>())
        {
            if (canvas != null)
            {
                if (canvas.name == "AuthorizationCanvas")
                {
                    _canvas = canvas;
                }
            }
        }

        _canvas.gameObject.SetActive(true);
    }

    public void ToSignInButtonAction()
    {
        GameObject.Find("AuthorizationCanvas").SetActive(false);

        foreach (Canvas canvas in Resources.FindObjectsOfTypeAll<Canvas>())
        {
            if (canvas != null)
            {
                if (canvas.name == "RegistrationCanvas")
                {
                    _canvas = canvas;
                }
            }
        }

        _canvas.gameObject.SetActive(true);
    }

    public void SignUpButtonAction()
    {
        SceneManager.LoadScene("NicknameEnteringScene");
    }

    public void ExitButtonAction()
    {
        SceneManager.LoadScene("ExitScene");
    }


    public void ExitYes()
    {
        Application.Quit();
    }

    public void ExitNo()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void RefreshSessionList()
    {
        SceneManager.LoadScene("SessionListMenu");
    }

    public void SetSessionListPagePlusOne()
    {
        SessionListLoadingMenu s = GameObject.Find("SessionListLoadingManager").GetComponent<SessionListLoadingMenu>();
        s.CurrentPage += 1;

        if (s.CurrentPage > s.lastPage)
        {
            s.CurrentPage = s.lastPage;
        }

        s.LoadPage();
    }

    public void SetSessionListPageMinusOne()
    {
        SessionListLoadingMenu s = GameObject.Find("SessionListLoadingManager").GetComponent<SessionListLoadingMenu>();
        s.CurrentPage -= 1;

        if (s.CurrentPage <= 0)
        {
            s.CurrentPage = 1;
        }

        s.LoadPage();
    }
}
