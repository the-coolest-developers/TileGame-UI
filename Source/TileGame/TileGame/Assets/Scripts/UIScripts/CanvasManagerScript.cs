using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManagerScript : MonoBehaviour
{
    public GameObject _canvasAuthorization;
    public GameObject _canvasRegistration;

    void Start()
    {
        _canvasRegistration.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
