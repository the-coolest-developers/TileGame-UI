using Unity.VisualScripting;
using UnityEngine;

public class PlacingManager : MonoBehaviour
{
    public GameObject[] _objects;
    private GameObject pendingObject;
    public Outline outline;
    public Canvas canvas;

    public float _rotationSpeed;

    private Vector3 _position;
    public float _rotationAmmount;
    private bool _isRotatable = true;

    [SerializeField] private LayerMask _layerMask;

    public float _gridSize;
    //[SerializeField] private Toggle _gridToggle;

    public bool _isPlacible;
    public bool _objectPlaced;
    public bool _enteredCollision;

    public float _smooth;

    int targetAngle = 0;
    int currentRotation = 0;

    void Update()
    {
        if (pendingObject == null)
        {
            return;
        }
        else
        { 
            pendingObject.GetComponent<Outline>().enabled = true;
            Cursor.visible = false;
        }

        pendingObject.transform.position = RoundToNearestGrid(_position);


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Destroy(pendingObject);
            canvas.GameObject().SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.R) && pendingObject != null && _isRotatable)
        {
            currentRotation++;

            targetAngle = currentRotation * 90;

            if (currentRotation == 4)
            {
                targetAngle = 0;
                currentRotation = 0;

            }

            _isPlacible = false;
        }

        if (pendingObject.transform.eulerAngles.y == targetAngle)
        {
            _isPlacible = true;
            _isRotatable = true;
        }
        else
        {
            _isRotatable = false;
            pendingObject.transform.rotation = Quaternion.RotateTowards(pendingObject.transform.rotation, Quaternion.Euler(0, targetAngle, 0), _smooth);
        }

        if (Input.GetMouseButtonDown(0) && _isPlacible && !_enteredCollision)
        {
            _objectPlaced = true;
        }

        if (_objectPlaced && pendingObject.transform.eulerAngles.y == targetAngle)
        {
            targetAngle = 0;
            currentRotation = 0;
            _isRotatable = true;
            PlaceObject();

            canvas.GameObject().SetActive(true);

            Cursor.visible = true;
        }

    }

    public void PlaceObject()
    {
        pendingObject.GetComponent<Outline>().enabled = false;
        pendingObject = null;
    }

    private void FixedUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit, 1000, _layerMask))
        {
            _position = hit.point;
        }
    }

    public void SelectObject(int index)
    {

        GameObject.Find("SelectionManager").GameObject().gameObject.ConvertTo<SelectionScript>().Deselect();

        pendingObject = Instantiate(_objects[index], _position, transform.rotation);

        canvas.GameObject().SetActive(false);

        outline = pendingObject.AddComponent<Outline>();
        outline.OutlineColor = Color.green;


        _objectPlaced = false;
    }
    Vector3 RoundToNearestGrid(Vector3 vector)
    {
        return new Vector3(
                  RoundToNearestGrid(vector.x),
                  RoundToNearestGrid(vector.y),
                  RoundToNearestGrid(vector.z));
    }

    float RoundToNearestGrid(float position)
    {
        float xDiff = position % _gridSize;

        position -= xDiff;
        if (xDiff > (_gridSize / 2))
        {
            position += _gridSize;
        }

        return position;
    }
}
