using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLookAtPlayerScript : MonoBehaviour
{
    public Transform _target;
    // Start is called before the first frame update
    void Start()
    {
        // enabled = true;
        _target = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(_target);
    }
}
