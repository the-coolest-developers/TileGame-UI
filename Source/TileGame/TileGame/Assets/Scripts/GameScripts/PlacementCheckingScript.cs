using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.TextCore.Text;
using UnityEngine;

public class PlacementCheckingScript : MonoBehaviour
{

    PlacingManager _placingManager;

    void Start()
    {
        _placingManager = GameObject.Find("PlacingManager").GetComponent<PlacingManager>();
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Object"))
        {
            _placingManager._enteredCollision = true;
            _placingManager.outline.OutlineColor = Color.red;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Object"))
        {
            _placingManager._enteredCollision = false;
            _placingManager.outline.OutlineColor = Color.green;
        }
    }

}
