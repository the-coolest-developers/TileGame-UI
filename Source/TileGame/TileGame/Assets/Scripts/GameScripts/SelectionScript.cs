using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Unity.VisualScripting;

public class SelectionScript : MonoBehaviour
{

    public GameObject _selectedObject;
    public Canvas _canvas;

    // Start is called before the first frame update
    void Start()
    {
        /*  PM = GameObject.FindGameObjectWithTag("PlacingManager");*/
        _canvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider.gameObject.CompareTag("Object") && GameObject.Find("PlacingManager").GameObject().gameObject.ConvertTo<PlacingManager>()._objectPlaced)
                {
                    SelectObject(hit.collider.gameObject);
                    ShowInfo();
                }
            }
        }

        if (Input.GetMouseButtonDown(1) && _selectedObject != null)
        {
            Deselect();
        }
    }

    private void SelectObject(GameObject obj)
    {
        if (obj == _selectedObject) return;
    
        Deselect();

        Outline outline = obj.GetComponent<Outline>();
        if (outline == null)
        {
            obj.AddComponent<Outline>();
        }
        else
        {
            outline.enabled = true;
            outline.OutlineColor = Color.yellow;
        }
        _selectedObject = obj;
    }

    public void Deselect()
    {
        if (_selectedObject != null)
        {
            _selectedObject.GetComponent<Outline>().enabled = false;
            _selectedObject = null;
            _canvas.enabled = false;
        }
    }

    private void ShowInfo()
    {
        _canvas.enabled = true;
        _canvas.transform.position = _selectedObject.transform.position;
        _canvas.transform.position += new Vector3(0, 5, 0);
    }
}
