using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Transform _cameraTransform;

    public float _movementSpeed = 1f;
    public float _movementTime = 1f;
    public float _rotationSpeed = 1f;
    public Vector3 _zoomSpeed;

    Vector3 movementVecotr;
    public Quaternion rotation;
    Vector3 zoom;

    public Vector3 _dragStartPosition;
    public Vector3 _dragPosition;

    // Start is called before the first frame update
    void Start()
    {
        movementVecotr = transform.position;
        rotation = transform.rotation;
        zoom = _cameraTransform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        HandleMovementInput();
        HandleMouseInput();
    }


    void HandleMouseInput()
    {
        if (Input.mouseScrollDelta.y != 0)
        {

            if (zoom.y + Input.mouseScrollDelta.y * -_zoomSpeed.y > new Vector3(0, 1, 0).y
                && zoom.y + Input.mouseScrollDelta.y * -_zoomSpeed.y < new Vector3(0, 30, 0).y)
            {
                zoom += Input.mouseScrollDelta.y * -_zoomSpeed;
            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            Plane plane = new Plane(Vector3.up, Vector3.zero);

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float entry;

            if (plane.Raycast(ray, out entry))
            {
                _dragStartPosition = ray.GetPoint(entry);
            }

        }

        if (Input.GetMouseButton(1))
        {
            Plane plane = new Plane(Vector3.up, Vector3.zero);

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float entry;

            if (plane.Raycast(ray, out entry))
            {
                _dragPosition = ray.GetPoint(entry);
                movementVecotr = transform.position + _dragStartPosition - _dragPosition;
            }

        }

    }

    void HandleMovementInput()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            movementVecotr += (transform.forward * _movementSpeed);
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            movementVecotr += (transform.right * -_movementSpeed);
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            movementVecotr += (transform.forward * -_movementSpeed);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            movementVecotr += (transform.right * _movementSpeed);
        }

        if (Input.GetKey(KeyCode.E))
        {
            rotation *= Quaternion.Euler(Vector3.up * -_rotationSpeed);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            rotation *= Quaternion.Euler(Vector3.up * _rotationSpeed);
        }

        if (Input.GetKey(KeyCode.Z))
        {

            if (zoom.y - _zoomSpeed.y > new Vector3(0, 1, 0).y)
            {
                zoom -= _zoomSpeed;
            }

        }

        if (Input.GetKey(KeyCode.X))
        {

            if (zoom.y + _zoomSpeed.y < new Vector3(0, 30, 0).y)
            {
                zoom += _zoomSpeed;
            }

        }

        transform.position = Vector3.Lerp(transform.position, movementVecotr, Time.deltaTime * _movementTime);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * _movementTime);
        _cameraTransform.localPosition = Vector3.Lerp(_cameraTransform.localPosition, zoom, Time.deltaTime * _movementTime);

    }
}
