﻿using TileGame.Application.Contracts.Http;
using TileGame.Application.Contracts.Json;
using TileGame.Application.Responses.Mappers;
using TileGame.Application.SingleSignOn;
using TileGame.Infrastructure.Common.Configuration;
using TileGame.Infrastructure.Http;
using TileGame.Infrastructure.Http.Mappers;
using TileGame.Infrastructure.Json;
using Zenject;

namespace Assembly_CSharp
{
    public class Startup : MonoInstaller
    {
        public override void InstallBindings()
        {
            var serviceUrlConfiguration = new ServiceUrlConfiguration("https://localhost:44317");
            Container.Bind<ServiceUrlConfiguration>().FromInstance(serviceUrlConfiguration);

            Container.Bind<IJsonConverter>().To<UnityJsonConverter>().AsSingle();
            Container.Bind<IHttpRequestSender>().To<UnityHttpRequestSender>().AsSingle();
            Container.Bind<IRequestStatusMapper>().To<RequestStatusMapper>().AsSingle();
            Container.Bind<IOperationMapper>().To<OperationMapper>().AsSingle();
            Container.Bind<ISingleSignOnService>().To<SingleSignOnService>().AsSingle();
        }
    }
}
