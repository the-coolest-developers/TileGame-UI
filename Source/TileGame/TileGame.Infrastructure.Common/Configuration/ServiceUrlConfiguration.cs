﻿namespace TileGame.Infrastructure.Common.Configuration
{
    public record class ServiceUrlConfiguration (string SingleSignOn);
}
