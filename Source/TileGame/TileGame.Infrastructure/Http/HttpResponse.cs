﻿using TileGame.Application.Contracts.Http;
using TileGame.Infrastructure.Common.Enums;

namespace TileGame.Infrastructure.Http
{
    public record class HttpResponse<TResponse>(ResponseStatus ResponseStatus, TResponse ResponseBody) : IHttpResponse<TResponse>;
}