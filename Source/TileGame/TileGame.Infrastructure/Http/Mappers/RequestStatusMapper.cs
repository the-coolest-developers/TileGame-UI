﻿using TileGame.Application.Commands;
using TileGame.Application.Contracts.Http;
using TileGame.Infrastructure.Common.Enums;

namespace TileGame.Infrastructure.Http.Mappers
{
    public class RequestStatusMapper : IRequestStatusMapper
    {
        //Pattern Matching is not supported in Unity :(
        private readonly Dictionary<ResponseStatus, OperationStatus> _statusDictionary = new()
        {
            {ResponseStatus.Success, OperationStatus.Success},
            {ResponseStatus.Created, OperationStatus.Success},
            {ResponseStatus.Accepted, OperationStatus.Success},

            {ResponseStatus.Conflict, OperationStatus.ValidationError},
            {ResponseStatus.InternalServerError, OperationStatus.ServerError},
        };

        public OperationStatus GetOperationStatus(ResponseStatus responseStatus)
        {
            return _statusDictionary.ContainsKey(responseStatus)
                ? _statusDictionary[responseStatus]
                : OperationStatus.UnknownError;
        }
    }
}
