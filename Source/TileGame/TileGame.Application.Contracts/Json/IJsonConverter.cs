﻿namespace TileGame.Application.Contracts.Json
{
    public interface IJsonConverter
    {
        TResponse? Deserialize<TResponse>(string json);
        string? Serialize<TResponse>(TResponse response);
    }
}
