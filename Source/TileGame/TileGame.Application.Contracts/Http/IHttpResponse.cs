﻿using TileGame.Infrastructure.Common.Enums;

namespace TileGame.Application.Contracts.Http;

public interface IHttpResponse<TResponse>
{
    ResponseStatus ResponseStatus { get; }
    TResponse? ResponseBody { get; }
}
