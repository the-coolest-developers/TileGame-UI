﻿namespace TileGame.Application.Contracts.Http;

public interface IHttpRequestSender
{
    IHttpResponse<TResponse> Get<TResponse>(string url);

    IHttpResponse<TResponse> Post<TResponse>(string url, object requestBody);
}
