﻿using TileGame.Application.Commands;

namespace TileGame.Application.Contracts.Http
{
    public interface IRequestStatusMapper
    {
        OperationStatus GetOperationStatus(Infrastructure.Common.Enums.ResponseStatus responseStatus);
    }
}
