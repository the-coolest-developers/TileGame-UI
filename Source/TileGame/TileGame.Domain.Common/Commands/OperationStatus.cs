﻿namespace TileGame.Application.Commands;

public enum OperationStatus
{
    Success,
    ValidationError,
    ServerError,
    ServiceUnavailable,
    UnknownError
}
